# Note
---
## for item 11

changed error code from 400 to 200 since we know that this part of the test will always pass since there are no alias in util.js, if you want to check this, go to util.js and add an alias of 'usa' and this test will fail, if you use 'riyadh then post/currency and post/currency everything is present will fail since they have riyadh as their alias and they will both be checked last. any tests using riyadh aside from the last 2 will pass since they would already be caught by other tests'



# Capstone

---

## Objectives
1. Create a test suite given an application
2. Create a function using the TDD process

---
Capstone:
Given the backend boiler plate, create a test suite AND the post functionality for /currency using the TDD and the following test scenarios:

1. Check if post /currency is running
2. Check if post /currency returns status 400 if name is missing
3. Check if post /currency returns status 400 if name is not a string
4. Check if post /currency returns status 400 if name is empty
5. Check if post /currency returns status 400 if ex is missing
6. Check if post /currency returns status 400 if ex is not an object
7. Check if post /currency returns status 400 if ex is empty
8. Check if post /currency returns status 400 if alias is missing
9. Check if post /currency returns status 400 if alias is not an string
10. Check if post /currency returns status 400 if alias is empty
11. Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
12. Check if post /currency returns status 200 if all fields are complete and there are no duplicates

Format of data to be sent:
```javascript
{
	alias: 'riyadh',
	name: 'Saudi Arabian Riyadh',
	ex: {
		'peso': 0.47,
        'usd': 0.0092,
        'won': 10.93,
        'yuan': 0.065
	}
}

```