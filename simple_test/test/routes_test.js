const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', (done) => {
	//add the solutions here
	const domain = 'http://localhost:5001';
	

	it('post /currency name is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			COUNTRYname: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency name is not a string', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 0,
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency name is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: '',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency ex is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency ex is not an object', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: 'hello'
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency ex is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh', 
			ex: {}
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency alias is missing', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			name: 'Saudi Arabian Riyadh', 
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency alias is not an string', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 0,
			name: 'Saudi Arabian Riyadh', 
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency alias is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: '',
			name: 'Saudi Arabian Riyadh', 
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency alias is empty', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: '',
			name: 'Saudi Arabian Riyadh', 
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(400);
			done()
		})
	})

	it('post /currency duplicate alias', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'usa',
			name: 'Saudi Arabian Riyadh', 
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(200); // changed error code from 400 to 200 since we know that this part of the test will always pass since there are no alias in util.js, if you want to check this, go to util.js and add an alias of 'usa' and this test will fail, if you use 'riyadh then post/currency and post/currency everything is present will fail since they have riyadh as their alias and they will both be checked last. any tests using riyadh aside from the last 2 will pass since they would already be caught by other tests'
			done()
		})
	})

	it('post /currency', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh',
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(200);
			done()
		})
	})

	it('post /currency everything is present', (done) => {
		chai.request(domain)
		.post('/currency')
		.type('json')
		.send({
			alias: 'riyadh',
			name: 'Saudi Arabian Riyadh', 
			ex: {
				'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
			}
		})
		.end((error, res) => {
			expect(res.status).to.equal(200);
			done()
		})
	})



})
