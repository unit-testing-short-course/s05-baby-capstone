const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {
		//create if conditions here to check the currency
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'Error': 'Bad Request - missing required parameters'
			})
		}

		if(typeof req.body.name != 'string'){
			return res.status(400).send({
				'Error': 'name is not string'
			})
		}

		if(req.body.name == ''){
			return res.status(400).send({
				'Error': 'Bad Request - missing required parameters'
			})
		}

		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'Error': 'Bad Request - missing required parameters'
			})
		}

		if(typeof req.body.ex != 'object'){
			return res.status(400).send({
				'Error': 'ex is not an object'
			})
		}

		if(Object.keys(req.body.ex).length == 0){
			return res.status(400).send({
				'Error': 'Bad Request - missing required parameters'
			})
		}

		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'Error': 'Bad Request - missing required parameters'
			})
		}

		if(typeof req.body.alias != 'string'){
			return res.status(400).send({
				'Error': 'alias is not string'
			})
		}

		if(req.body.alias == ''){
			return res.status(400).send({
				'Error': 'Bad Request - missing required parameters'
			})
		}

		check = Object.values(exchangeRates).find((obj) => {
			return obj.alias == req.body.alias
		});
		if(typeof check === 'object'){
			return res.status(400).send({
				'Error': 'Bad Request - there is a duplicate value'
			})
		}

		//return status 200 if route is running
		return res.status(200).send({
			'Message': 'Route is running'
		})
	})
}
